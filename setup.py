import setuptools
with open("README.md", "r") as fh:
   long_description = fh.read()
setuptools.setup(
   name="fbx-Utilities-SolleXelloS", # Replace with your own username
   version="0.0.1",
   author="Evan McCall",
   author_email="evan@digital-giant.com",
   description="A package of sample code with FBX SDK for Python 2020.1",
   long_description=long_description,
   long_description_content_type="text/markdown",
   url="https://bitbucket.org/SolleXelloS/fbxsdk-pythonbinding27-utilities-package-dist/src", # Change Github
   packages=setuptools.find_packages(),
   classifiers=[
       "Programming Language :: Python :: 2", # Replace with Python Interpretter
       "License :: OSI Approved :: MIT License",
       "Operating System :: OS Independent",
   ],
   python_requires='>=2.7', # Replace with Python Interpretter 
)
